<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>index.jsp</title>
</head>
<body>
	
	<h3>Please choose parser for building tariff collection: </h3>
	<form action="controller" method="post">
		<input type="hidden" name="command" value="parse" />
		<input type="radio" name="parser_choice" value="SAX" />SAX parser<br />
		<input type="radio" name="parser_choice" value="StAX" />StAX parser<br />
		<input type="radio" name="parser_choice" value="DOM" />DOM parser<br /><hr />
		<input type="submit" name="parsing_button" value="Parse" />
	</form>	
</body>
</html>