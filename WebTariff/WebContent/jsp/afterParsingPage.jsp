<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="by.epam.web_tariff.entity.CallParameterName" %>
<%@ page import="by.epam.web_tariff.entity.InternetParameterName" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tariff set building results</title>
<style>
	caption {
		font-size: large;
		font-style: italic;
		text-align: left;
	}
	table, th, td {
		border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td {
	    padding: 5px;
	}
	tr:nth-child(even) {
	background-color: #eee; 
	}
</style>
</head>
<body>
	<h3>Tariffs denied by ${requestScope.parserChoice} while parsing:</h3>
	<table>
		<tr>
			<th>Tariff id</th>
			<th>Reason</th>
		</tr>
	<c:forEach var="entry" items="${deniedTariffs}">
		<tr>
			<td>${entry.key.tariffId}</td>
			<td>${entry.value}</td>
		</tr>
	</c:forEach>
	</table><hr />
	
	<h3>Tariff set made by ${requestScope.parserChoice}: </h3>
	<table>
		<caption>Call tariffs</caption>
		<tr>
			<th rowspan="2">Tariff ID</th>
			<th rowspan="2">Operator name</th>
			<th rowspan="2">Tariff name</th>
			<th rowspan="2">Connection fee, BYR</th>
			<th rowspan="2">Initial payment, BYR</th>
			<th rowspan="2">Default Internet speed, Mb/s</th>
			<th rowspan="2">SMS price, BYR</th>
			<th rowspan="2">MMS price, BYR</th>
			<th colspan="5">Native network parameters</th>
			<th colspan="5">Stationary network parameters</th>
			<th colspan="5">Other network parameters</th>
			<th rowspan="2">Every megabyte price, BYR</th>
		</tr>
		<tr>
			<th>Minutes with discount</th>
			<th>Minutes without discount</th>
			<th>Discount minute price, BYR</th>
			<th>Common minute price, BYR</th>
			<th>Minute type, (call, day, month)</th>
			<th>Minutes with discount</th>
			<th>Minutes without discount</th>
			<th>Discount minute price, BYR</th>
			<th>Common minute price, BYR</th>
			<th>Minute type, (call, day, month)</th>
			<th>Minutes with discount</th>
			<th>Minutes without discount</th>
			<th>Discount minute price, BYR</th>
			<th>Common minute price, BYR</th>
			<th>Minute type, (call, day, month)</th>
		</tr>
		<c:forEach var="tariff" items="${callTariffSet}">
		<tr>
			<td>${tariff.tariffId}</td>
			<td>${tariff.operatorName}</td>
			<td>${tariff.tariffName}</td>
			<td>${tariff.connectionFee} BYR</td>
			<td>${tariff.initialPayment} BYR</td>
			<td>${tariff.internetSpeedDefault} Mb/s</td>
			<td>${tariff.smsPrice} BYR</td>
			<td>${tariff.mmsPrice} BYR</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.MINUTES_WITH_DISCOUNT)}</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT)}</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.DISCOUNT_MINUTE_PRICE)} BYR</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.COMMON_MINUTE_PRICE)} BYR</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.MINUTE_TYPE)} type</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.MINUTES_WITH_DISCOUNT)}</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT)}</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.DISCOUNT_MINUTE_PRICE)} BYR</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.COMMON_MINUTE_PRICE)} BYR</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.MINUTE_TYPE)} type</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.MINUTES_WITH_DISCOUNT)}</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT)}</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.DISCOUNT_MINUTE_PRICE)} BYR</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.COMMON_MINUTE_PRICE)} BYR</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.MINUTE_TYPE)} type</td>
			<td>${tariff.everyMegabytePrice} BYR</td>		
		</tr>
		</c:forEach>
	</table>
	<hr /><br /><br />	
	<table>
		<caption>Internet tariffs</caption>
		<tr>
			<th rowspan="2">Tariff ID</th>
			<th rowspan="2">Operator name</th>
			<th rowspan="2">Tariff name</th>
			<th rowspan="2">Connection fee, BYR</th>
			<th rowspan="2">Initial payment, BYR</th>
			<th rowspan="2">Default Internet speed, Mb/s</th>
			<th rowspan="2">SMS price, BYR</th>
			<th rowspan="2">MMS price, BYR</th>
			<th colspan="5">Native network parameters</th>
			<th colspan="5">Stationary network parameters</th>
			<th colspan="5">Other network parameters</th>
			<th colspan="6">Internet parameters</th>
			<th rowspan="2">Max Internet speed, Mb/s</th>
		</tr>
		<tr>
			<th>Minutes with discount</th>
			<th>Minutes without discount</th>
			<th>Discount minute price, BYR</th>
			<th>Common minute price, BYR</th>
			<th>Minute type, (call, day, month)</th>
			<th>Minutes with discount</th>
			<th>Minutes without discount</th>
			<th>Discount minute price, BYR</th>
			<th>Common minute price, BYR</th>
			<th>Minute type, (call, day, month)</th>
			<th>Minutes with discount</th>
			<th>Minutes without discount</th>
			<th>Discount minute price, BYR</th>
			<th>Common minute price, BYR</th>
			<th>Minute type, (call, day, month)</th>
			<th>Megabytes with discount, MB</th>
			<th>Discount megabyte price, BYR</th>
			<th>Common megabyte price, BYR</th>
			<th>Amount of traffic, MB</th>
			<th>Amount of fast traffic, MB</th>
			<th>Megabyte type (day, month)</th>
		</tr>
		<c:forEach var="tariff" items="${internetTariffSet}">
		<tr>
			<td>${tariff.tariffId}</td>
			<td>${tariff.operatorName}</td>
			<td>${tariff.tariffName}</td>
			<td>${tariff.connectionFee}</td>
			<td>${tariff.initialPayment}</td>
			<td>${tariff.internetSpeedDefault}</td>
			<td>${tariff.smsPrice}</td>
			<td>${tariff.mmsPrice}</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.MINUTES_WITH_DISCOUNT)}</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT)}</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.DISCOUNT_MINUTE_PRICE)} BYR</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.COMMON_MINUTE_PRICE)} BYR</td>
			<td>${tariff.nativePriceTable.get(CallParameterName.MINUTE_TYPE)} type</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.MINUTES_WITH_DISCOUNT)}</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT)}</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.DISCOUNT_MINUTE_PRICE)} BYR</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.COMMON_MINUTE_PRICE)} BYR</td>
			<td>${tariff.stationaryPriceTable.get(CallParameterName.MINUTE_TYPE)} type</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.MINUTES_WITH_DISCOUNT)}</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT)}</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.DISCOUNT_MINUTE_PRICE)} BYR</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.COMMON_MINUTE_PRICE)} BYR</td>
			<td>${tariff.otherNetworkPriceTable.get(CallParameterName.MINUTE_TYPE)} type</td>
			<td>${tariff.internetPriceTable.get(InternetParameterName.MEGABYTES_WITH_DISCOUNT)} MB</td>
			<td>${tariff.internetPriceTable.get(InternetParameterName.DISCOUNT_MEGABYTE_PRICE)} BYR</td>
			<td>${tariff.internetPriceTable.get(InternetParameterName.COMMON_MEGABYTE_PRICE)} BYR</td>
			<td>${tariff.internetPriceTable.get(InternetParameterName.AMOUNT_OF_TRAFFIC)} MB</td>
			<td>${tariff.internetPriceTable.get(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC)} MB</td>
			<td>${tariff.internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE)} type</td>
			<td>${tariff.internetSpeedMax} Mb/s</td>
		</tr>
		</c:forEach>
	</table>
	<hr /><br />
	<a href="index.jsp"><button>Go back</button></a>
</body>
</html>