package by.epam.web_tariff.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

public class XSDValidationService {
	
	private static final Logger LOG = Logger.getLogger(XSDValidationService.class);
	
	public static boolean validateByXSD(String fileName, String schemaLocation) {
		boolean validationResult = false;
		
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");		// Создаём реальную объект-фабрику для создания объектов-схем
		
		File schemaFile = new File(schemaLocation);													// Создаём объект-файл, ссылающийся на XSD-файл по определённому пути
		
		Schema schema = null;
		try {
			schema = factory.newSchema(schemaFile);													// Получаем от фабрики реальный объект-схему, ссылающийся на файл с XSD-схемой. Может выбросить SAXException.
		} catch (SAXException ex) {
			LOG.error("Some errors occured while creating schema-object");
		}
		
		Validator validator = schema.newValidator();												// Создаём объект-валидатор, который будет руководствоваться созданной XSD-схемой

		try {
			InputStream input = new FileInputStream(fileName);										// Создаём объект-источник потока данных для валидации (т.е. то, что будем валидировать)
			Source source = new StreamSource(input);
			validator.validate(source);																// Начинаем валидацию xml-файла
			validationResult = true;
			LOG.info("Document is valid!");
		} catch (SAXException | IOException ex) {
			LOG.error("Document is not valid because: " + ex.getMessage());
		}
		return validationResult;
	}
}
