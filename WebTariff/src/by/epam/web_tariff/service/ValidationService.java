package by.epam.web_tariff.service;

import java.util.Map;

import by.epam.web_tariff.entity.CallParameterName;
import by.epam.web_tariff.entity.InternetParameterName;
import by.epam.web_tariff.entity.TariffName;

public class ValidationService {
	private static final String INCORRECT_TARIFF_ID = "incorrect tariff id";
	private static final String INCORRECT_OPERATOR_NAME = "incorrect operator name";
	private static final String INCORRECT_TARIFF_NAME = "incorrect tariff name";
	private static final String INCORRECT_CONNECTION_FEE = "incorrect connection fee";
	private static final String INCORRECT_INITIAL_PAYMENT = "incorrect initial payment";
	private static final String INCORRECT_INTERNET_SPEED_DEFAULT = "incorrect internet speed default";
	private static final String INCORRECT_INTERNET_SPEED_MAX = "incorrect internet speed max";
	private static final String INCORRECT_SMS_PRICE = "incorrect sms price";
	private static final String INCORRECT_MMS_PRICE = "incorrect mms price";
	private static final String INCORRECT_EVERY_MEGABYTE_PRICE = "incorrect every megabyte price";
	private static final String INCORRECT_NATIVE_NETWORK_CALL_PARAMETERS = "incorrect native network call parameters";
	private static final String INCORRECT_STATIONARY_NETWORK_CALL_PARAMETERS = "incorrect stationary network call parameters";
	private static final String INCORRECT_OTHER_NETWORK_CALL_PARAMETERS = "incorrect other network call parameters";
	private static final String INCORRECT_INTERNET_PARAMETERS = "incorrect internet parameters";
	
	// проверка полей тарифов для звонков
	public static String validate(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice, int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		String result = validateCommonPart(tariffId, operatorName, tariffName, connectionFee, 
											initialPayment, internetSpeedDefault, smsPrice, mmsPrice,  
											nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
		if (result == null) {
			if (everyMegabytePrice < 0) {
				return INCORRECT_EVERY_MEGABYTE_PRICE;
			}
		}				
		return result;
	}
	
	// проверка полей тарифов для интернета
	public static String validate(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
									   int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
									   Map<CallParameterName, Object> nativePriceTable, 
									   Map<CallParameterName, Object> stationaryPriceTable, 
									   Map<CallParameterName, Object> otherNetworkPriceTable, 
									   Map<InternetParameterName, Object> internetPriceTable) {
		
		String result = validateCommonPart(tariffId, operatorName, tariffName, connectionFee, 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice,  
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
		if (result == null) {
			if (internetSpeedMax < 0) {
				return INCORRECT_INTERNET_SPEED_MAX;
			}
			
			if (internetPriceTable.get(InternetParameterName.MEGABYTES_WITH_DISCOUNT) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.MEGABYTES_WITH_DISCOUNT) < 0) {
					return INCORRECT_INTERNET_PARAMETERS;
				}
			} else {
				internetPriceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.DISCOUNT_MEGABYTE_PRICE) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.DISCOUNT_MEGABYTE_PRICE) < 0) {
					return INCORRECT_INTERNET_PARAMETERS;
				}
			} else {
				internetPriceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.COMMON_MEGABYTE_PRICE) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.COMMON_MEGABYTE_PRICE) < 0) {
					return INCORRECT_INTERNET_PARAMETERS;
				}
			} else {
				internetPriceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.AMOUNT_OF_TRAFFIC) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.AMOUNT_OF_TRAFFIC) < 0) {
					return INCORRECT_INTERNET_PARAMETERS;
				}
			} else {
				internetPriceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.valueOf(0));
			}
			
			if (internetPriceTable.get(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC) != null) {
				if ((Integer)internetPriceTable.get(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC) < 0) {
					return INCORRECT_INTERNET_PARAMETERS;
				}
			} else {
				internetPriceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.valueOf(0));
			}
						
			if (internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE) != null) {
				if (!"day".equals(internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE)) && 
					!"month".equals(internetPriceTable.get(InternetParameterName.MEGABYTE_TYPE))) {
					return INCORRECT_INTERNET_PARAMETERS;
				}
			} else {
				internetPriceTable.put(InternetParameterName.MEGABYTE_TYPE, "month");
			}
			
		}			
		return result;
	}
	
	// проверка полей общих для всех тарифов
	public static String validateCommonPart(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
							  int internetSpeedDefault, int smsPrice, int mmsPrice, 
							  Map<CallParameterName, Object> nativePriceTable, 
							  Map<CallParameterName, Object> stationaryPriceTable, 
							  Map<CallParameterName, Object> otherNetworkPriceTable) {
		String result = null;

		if ((tariffId == null) || tariffId.isEmpty()) {
			return INCORRECT_TARIFF_ID;
		}
		
		if (!"MTS".equals(operatorName)) {
			return INCORRECT_OPERATOR_NAME;
		}
		
		if (tariffName == null) {
			return INCORRECT_TARIFF_NAME;
		}
		
		if (connectionFee < 0) {
			return INCORRECT_CONNECTION_FEE;
		}
		
		if (initialPayment < 0) {
			return INCORRECT_INITIAL_PAYMENT;
		}
		
		if (internetSpeedDefault < 0) {
			return INCORRECT_INTERNET_SPEED_DEFAULT;
		}
		
		if (smsPrice < 0) {
			return INCORRECT_SMS_PRICE;
		}
		
		if (mmsPrice < 0) {
			return INCORRECT_MMS_PRICE;
		}
		
		if (!validateMapFields(nativePriceTable)) {
			return INCORRECT_NATIVE_NETWORK_CALL_PARAMETERS;
		}
		
		if (!validateMapFields(stationaryPriceTable)) {
			return INCORRECT_STATIONARY_NETWORK_CALL_PARAMETERS;
		}
		
		if (!validateMapFields(otherNetworkPriceTable)) {
			return INCORRECT_OTHER_NETWORK_CALL_PARAMETERS;
		}
		return result;
	}

	// проверка полей типа Map для звонков
	public static boolean validateMapFields(Map<CallParameterName, Object> table) {
		boolean result = true; 
		
		if (table.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT) != null) {
			if ((Integer)table.get(CallParameterName.MINUTES_WITHOUT_DISCOUNT) < 0) {
				return false;
			}
		} else {
			table.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.MINUTES_WITH_DISCOUNT) != null) {
			if ((Integer)table.get(CallParameterName.MINUTES_WITH_DISCOUNT) < 0) {
				return false;
			}
		} else {
			table.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.DISCOUNT_MINUTE_PRICE) != null) {
			if ((Integer)table.get(CallParameterName.DISCOUNT_MINUTE_PRICE) < 0) {
				return false;
			}
		} else {
			table.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.COMMON_MINUTE_PRICE) != null) {
			if ((Integer)table.get(CallParameterName.COMMON_MINUTE_PRICE) < 0) {
				return false;
			}
		} else {
			table.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.valueOf(0));
		}
		
		if (table.get(CallParameterName.MINUTE_TYPE) != null) {
			if (!"call".equals(table.get(CallParameterName.MINUTE_TYPE)) &&
				!"day".equals(table.get(CallParameterName.MINUTE_TYPE)) &&
				!"month".equals(table.get(CallParameterName.MINUTE_TYPE))) {
				return false;
			}
		} else {
			table.put(CallParameterName.MINUTE_TYPE, "call");
		}
		return result;
	}

}
