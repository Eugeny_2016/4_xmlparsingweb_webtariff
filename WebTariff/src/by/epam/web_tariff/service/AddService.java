package by.epam.web_tariff.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import by.epam.web_tariff.entity.CallParameterName;
import by.epam.web_tariff.entity.CallTariff;
import by.epam.web_tariff.entity.InternetParameterName;
import by.epam.web_tariff.entity.InternetTariff;
import by.epam.web_tariff.entity.Tariff;
import by.epam.web_tariff.entity.TariffName;

public class AddService {
	// вставка тарифа для звонков
	public static boolean addTariff(Set<Tariff> tariffSet, String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice,int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		Tariff tariff = new CallTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment,
									   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice,
									   new HashMap<CallParameterName, Object>(nativePriceTable), 
									   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
									   new HashMap<CallParameterName, Object>(otherNetworkPriceTable));				// здесь именно new должен быть, иначе присвоится ссылка на поле из хендлера, которое постоянно меняется
		return tariffSet.add(tariff);																				// и в итоге получишь у всех тарифов в MAP-полях все одинаковые (последние распарсенные) значения!!!
	}																												// будь внимателен со ссылочными типами данных!!!
																													// со String такое не случается, потому что это immutable тип, как int, long и прочее. Поэтому новый объект и так сам всегда создаётся.
																													// а с другими ссылочными типами, нужно самому создать новые объеты и только потом укладывать их в новый тариф!!!
	// вставка тарифа для интернета
	public static boolean addTariff(Set<Tariff> tariffSet, String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
								   int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable, 
								   Map<InternetParameterName, Object> internetPriceTable) {
		
		Tariff tariff = new InternetTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
										   internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax,
										   new HashMap<CallParameterName, Object>(nativePriceTable), 
										   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
										   new HashMap<CallParameterName, Object>(otherNetworkPriceTable), 
										   new HashMap<InternetParameterName, Object>(internetPriceTable));
		return tariffSet.add(tariff);
	}
	
	// вставка неправильного тарифа для звонков
	public static void addDeniedTariff(Map<Tariff, String> deniedTariffs, String reason, String tariffId, String operatorName, TariffName tariffName, 
										  int connectionFee, int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice,int everyMegabytePrice,  
										  Map<CallParameterName, Object> nativePriceTable, 
										  Map<CallParameterName, Object> stationaryPriceTable, 
										  Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		Tariff tariff = new CallTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment,
									   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice,
									   new HashMap<CallParameterName, Object>(nativePriceTable), 
									   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
									   new HashMap<CallParameterName, Object>(otherNetworkPriceTable));
		deniedTariffs.put(tariff, reason);
	}																												

	// вставка неправильного тарифа для интернета
	public static void addDeniedTariff(Map<Tariff, String> deniedTariffs, String reason, String tariffId, String operatorName, TariffName tariffName, 
										  int connectionFee, int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
										  Map<CallParameterName, Object> nativePriceTable, 
										  Map<CallParameterName, Object> stationaryPriceTable, 
										  Map<CallParameterName, Object> otherNetworkPriceTable, 
										  Map<InternetParameterName, Object> internetPriceTable) {
		
		Tariff tariff = new InternetTariff(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
										   internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax,
										   new HashMap<CallParameterName, Object>(nativePriceTable), 
										   new HashMap<CallParameterName, Object>(stationaryPriceTable), 
										   new HashMap<CallParameterName, Object>(otherNetworkPriceTable), 
										   new HashMap<InternetParameterName, Object>(internetPriceTable));
		deniedTariffs.put(tariff, reason);
	}
}
