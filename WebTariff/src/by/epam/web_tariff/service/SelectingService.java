package by.epam.web_tariff.service;

import java.util.HashSet;
import java.util.Set;

import by.epam.web_tariff.entity.Tariff;

public class SelectingService {
	
	public static Set<Tariff> selectTariffsByClass(Set<Tariff> tariffSet, Class<? extends Tariff> className) {
		Set<Tariff> selectedSet = new HashSet<Tariff>();
		for (Tariff tariff : tariffSet) {
			if (className == tariff.getClass()) {
				selectedSet.add(tariff);
			}
		}		
		return selectedSet;
	}
}
