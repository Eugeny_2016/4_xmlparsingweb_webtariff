package by.epam.web_tariff.service.parsing.stax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import by.epam.web_tariff.entity.CallParameterName;
import by.epam.web_tariff.entity.InternetParameterName;
import by.epam.web_tariff.entity.TariffName;
import by.epam.web_tariff.service.parsing.TariffCollectionBuilder;
import by.epam.web_tariff.service.parsing.TariffsTagName;
import by.epam.web_tariff.service.AddService;
import by.epam.web_tariff.service.ValidationService;

@SuppressWarnings("incomplete-switch")
public class StAXTariffCollectionBuilder extends TariffCollectionBuilder {
	
	private static final Logger LOG = Logger.getLogger(StAXTariffCollectionBuilder.class);
	private XMLInputFactory inputFactory;
	
	public StAXTariffCollectionBuilder() {
		inputFactory = XMLInputFactory.newInstance();
	}
	
	@Override
	public void buildTariffSet(String fileName) {
		InputStream input = null;
		XMLStreamReader reader = null;
		try {
			input = new FileInputStream(fileName);										// создаём объект файлового входного потока нашего хмл-документа			
			reader = inputFactory.createXMLStreamReader(input);							// создаём объект-парсер вида StAX
			buildTariff(reader);														// передаём объект-парсер в метод, управляющий самим парсингом и возвращающий список тарифов
			LOG.info("StAX parsing process has come to an end");
		} catch (FileNotFoundException ex) {
			LOG.error("File wasn't found while parsing with StAX-parser" + "\n" + ex);
		} catch (XMLStreamException ex) {
			LOG.error("Some XMLStream exception has occured while parsing with StAX-parser" + "\n" + ex);
		}		
	}
	
	private void buildTariff(XMLStreamReader reader) throws XMLStreamException {
		TariffsTagName tagName = null;
		int type = reader.getEventType();
		boolean nativeFlag = false;
		boolean stationaryFlag = false;
		boolean othernetFlag = false;
		boolean insertResult = false;
		String negativeValidationReason = null;
		String wrongTariffName = null;
		String reason = null;
		
		// список полей из классов, которые будут принимать в себя значения в процессе парсинга 
		String tariffId = null;																							// Tariff
		String operatorName = null;
		TariffName tariffName = null;
		int connectionFee = 0;
		int initialPayment = 0;
		int internetSpeedDefault = 0;
		int smsPrice = 0;
		int mmsPrice = 0;
		Map<CallParameterName, Object> nativePriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> otherNetworkPriceTable = new HashMap<CallParameterName, Object>();
		int everyMegabytePrice = 0;																						// CallTariff
		Map<InternetParameterName, Object> internetPriceTable = new HashMap<InternetParameterName, Object>(); 		// InternetTariff
		int internetSpeedMax = 0;
				
		while (reader.hasNext()) {														// начинаем парсинг хмл-документа
			try {
				switch (type) {																// определяем тип прочитанного элемента
				case XMLStreamConstants.START_DOCUMENT:
					LOG.info("StAX parsing process has started");
					break;
				case XMLStreamConstants.START_ELEMENT:
					tagName = TariffsTagName.valueOf(reader.getLocalName().toUpperCase().replace("-", "_"));
					switch (tagName) {
						case CALL_TARIFF:
						case INTERNET_TARIFF:
							tariffId = reader.getAttributeValue(null, "id");
							break;
						case NATIVE_NETWORK:
							nativeFlag = true;
							break;
						case STATIONARY_NETWORK:
							stationaryFlag = true;
							break;
						case OTHER_NETWORK:
							othernetFlag = true;
							break;
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					String text = reader.getText().trim();
					if(text.isEmpty()) {
						break;
					}
					switch (tagName) {
					case OPERATOR_NAME:
						operatorName = text;
						break;
					case TARIFF_NAME:
						try {
							tariffName = TariffName.valueOf(text.toUpperCase().replace(' ', '_'));
						} catch (IllegalArgumentException ex) {
							tariffName = TariffName.UNKNOWN;
							wrongTariffName = text.toString();
						}
						break;
					case CONNECTION_FEE:
						connectionFee = Integer.parseInt(text);
						break;
					case INITIAL_PAYMENT:
						initialPayment = Integer.parseInt(text);
						break;
					case INTERNET_SPEED_DAFAULT:
						internetSpeedDefault = Integer.parseInt(text);
						break;
					case INTERNET_SPEED_MAX:
						internetSpeedMax = Integer.parseInt(text);
						break;
					case EVERY_MEGABYTE_PRICE:
						everyMegabytePrice = Integer.parseInt(text);
						break;
					case SMS:
						smsPrice = Integer.parseInt(text);
						break;
					case MMS:
						mmsPrice = Integer.parseInt(text);
						break;
					case MINUTES_WITHOUT_DISCOUNT:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text));
						}
						break;
					case MINUTES_WITH_DISCOUNT:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text));
						}
						break;
					case DISCOUNT_MINUTE_PRICE:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text));
						}
						break;
					case COMMON_MINUTE_PRICE:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text));
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text));
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text));
						}
						break;
					case MINUTE_TYPE:
						if (nativeFlag) {
							nativePriceTable.put(CallParameterName.MINUTE_TYPE, text);
						} else if (stationaryFlag) {
							stationaryPriceTable.put(CallParameterName.MINUTE_TYPE, text );
						} else if (othernetFlag) {
							otherNetworkPriceTable.put(CallParameterName.MINUTE_TYPE, text);
						}
						break;
					case MEGABYTES_WITH_DISCOUNT:
						internetPriceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.parseInt(text));
						break;
					case DISCOUNT_MEGABYTE_PRICE:
						internetPriceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.parseInt(text));
						break;
					case COMMON_MEGABYTE_PRICE:
						internetPriceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.parseInt(text));
						break;
					case AMOUNT_OF_TRAFFIC:
						internetPriceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.parseInt(text));
						break;
					case AMOUNT_OF_FAST_TRAFFIC:
						internetPriceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.parseInt(text));
						break;
					case MEGABYTE_TYPE:
						internetPriceTable.put(InternetParameterName.MEGABYTE_TYPE, text);
						break;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					tagName = TariffsTagName.valueOf(reader.getLocalName().toUpperCase().replace("-", "_"));
					switch (tagName) {
					case NATIVE_NETWORK:
						nativeFlag = false;
						break;
					case STATIONARY_NETWORK:
						stationaryFlag = false;
						break;
					case OTHER_NETWORK:
						othernetFlag = false;
						break;
					case CALL_TARIFF:
						if (tariffName == TariffName.UNKNOWN) {
							LOG.info("Tariff " + wrongTariffName + " denied!");
							reason = "wrong tariff name";
							AddService.addDeniedTariff(deniedTariffs, reason, tariffId, operatorName, tariffName, connectionFee, initialPayment,		// если тариф с неизвестным именем, добавляем его в набор отклонённых тарифов 
												 	   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
												 	   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
							break;
						}
						// отправляем данные на валидацию
						negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																 internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
																 nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						// если валидация прошла успешно, отправляем данные сервису вставки тарифа в набор
						if (negativeValidationReason == null) {
							insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
																nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
							LOG.debug("Validation result is true!");
							if (insertResult) {
								LOG.debug("Insertion result is true!");
								LOG.info("Tariff " + tariffName + " accepted!");
							} else {
								LOG.debug("Insertion result is false!");
								LOG.info("Tariff " + tariffName + " denied!");
							}
							
						} else {
							LOG.debug("Validation result is false!");
							LOG.info("Tariff " + tariffName + " denied!");
							AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 		// добавляем неправильный тариф в набор отклонённых тарифов 
													   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
													   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						}
						clearVariables(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, smsPrice, mmsPrice,		// обнуляем значения всех временных полей 
								   everyMegabytePrice, nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						break;
					case INTERNET_TARIFF:
						if (tariffName == TariffName.UNKNOWN) {
							LOG.info("Tariff " + wrongTariffName + " denied!");
							AddService.addDeniedTariff(deniedTariffs, reason, tariffId, operatorName, tariffName, connectionFee, initialPayment,		// если тариф с неизвестным именем, добавляем его в набор отклонённых тарифов 
									 				   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
									 				   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
							break;
						}
						// отправляем данные на валидацию
						negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, 
																nativePriceTable, stationaryPriceTable, otherNetworkPriceTable,
																internetPriceTable);
						// если валидация прошла успешно, отправляем данные сервису вставки тарифа в набор
						if (negativeValidationReason == null) {
							insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
																internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, 
																nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, 
																internetPriceTable);
							LOG.debug("Validation result is true!");
							if (insertResult) {
								LOG.debug("Insertion result is true!");
								LOG.info("Tariff " + tariffName + " accepted!");
							} else {
								LOG.debug("Insertion result is false!\n");
								LOG.info("Tariff " + tariffName + " denied!");
							}
							
						} else {
							LOG.debug("Validation result is false!");
							LOG.info("Tariff " + tariffName + " denied!");
							AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee,		// добавляем неправильный тариф в набор отклонённых тарифов 
									   				   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
									   				   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
						}
						clearVariables(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, smsPrice, mmsPrice,		// обнуляем значения всех временных полей 
								   internetSpeedMax, nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
						break;
					}
					break;
				}
			} catch (NumberFormatException ex) {
				LOG.info("Incorrect content! Tariff id: " + tariffId + " xml-element: " + tagName);		// если каким-то образом в  xml-элементе оказались данные неверного формата
			}
			type = reader.next();																		// переходим к следующему элементу хмл-документа
		}
	}
	
	// метод по обнулению всех полей, которые используются при парсинге call-tariff-ов
	private void clearVariables(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice, int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		tariffId = null; 
		operatorName = null; 
		tariffName = null; 
		connectionFee = 0; initialPayment = 0; internetSpeedDefault = 0; smsPrice = 0; mmsPrice = 0; everyMegabytePrice = 0;
		nativePriceTable.clear(); stationaryPriceTable.clear(); otherNetworkPriceTable.clear();
	}
		
	// метод по обнулению всех полей, которые используются при парсинге internet-tariff-ов
	private void clearVariables(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
								   int internetSpeedDefault, int internetSpeedMax, int smsPrice, int mmsPrice,
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable, 
								   Map<InternetParameterName, Object> internetPriceTable) {
		tariffId = null; 
		operatorName = null; 
		tariffName = null; 
		connectionFee = 0; initialPayment = 0; internetSpeedDefault = 0; smsPrice = 0; mmsPrice = 0; internetSpeedMax = 0;
		nativePriceTable.clear(); stationaryPriceTable.clear(); otherNetworkPriceTable.clear(); internetPriceTable.clear();
	}
}
