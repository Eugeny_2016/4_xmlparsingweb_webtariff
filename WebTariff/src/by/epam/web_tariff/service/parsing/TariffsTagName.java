package by.epam.web_tariff.service.parsing;

public enum TariffsTagName {
	TARIFFS, 
	CALL_TARIFF, 
	INTERNET_TARIFF,
	OPERATOR_NAME, 
	TARIFF_NAME, 
	CONNECTION_FEE, 
	INITIAL_PAYMENT,
	INTERNET_SPEED_DAFAULT, 
	INTERNET_SPEED_MAX, 
	SMS, MMS,
	CALL_PRICES, 
	MOBILE_INTERNET, 
	EVERY_MEGABYTE_PRICE,
	NATIVE_NETWORK, STATIONARY_NETWORK, OTHER_NETWORK, 
	MINUTES_WITHOUT_DISCOUNT, MINUTES_WITH_DISCOUNT,
	DISCOUNT_MINUTE_PRICE, COMMON_MINUTE_PRICE, MINUTE_TYPE, 
	DISCOUNT_MEGABYTE_PRICE, COMMON_MEGABYTE_PRICE, MEGABYTE_TYPE, 
	MEGABYTES_WITH_DISCOUNT, AMOUNT_OF_TRAFFIC, AMOUNT_OF_FAST_TRAFFIC
}
