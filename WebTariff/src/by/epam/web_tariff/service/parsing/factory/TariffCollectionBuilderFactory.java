package by.epam.web_tariff.service.parsing.factory;

import by.epam.web_tariff.exception.TariffException;
import by.epam.web_tariff.service.parsing.TariffCollectionBuilder;
import by.epam.web_tariff.service.parsing.dom.DOMTariffCollectionBuilder;
import by.epam.web_tariff.service.parsing.sax.SAXTariffCollectionBuilder;
import by.epam.web_tariff.service.parsing.stax.StAXTariffCollectionBuilder;

public class TariffCollectionBuilderFactory {
	
	public TariffCollectionBuilder getTariffBuilder(String parserType) throws TariffException {
		ParserType type = ParserType.valueOf(parserType.toUpperCase());
		switch (type) {
		case SAX:
			return new SAXTariffCollectionBuilder();
		case STAX:
			return new StAXTariffCollectionBuilder();
		case DOM:
			return new DOMTariffCollectionBuilder();
		default:
			throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
		}
	}
}

enum ParserType {
	SAX, STAX, DOM
}