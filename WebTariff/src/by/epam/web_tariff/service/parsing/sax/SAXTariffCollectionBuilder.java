package by.epam.web_tariff.service.parsing.sax;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.epam.web_tariff.exception.TariffException;
import by.epam.web_tariff.service.parsing.TariffCollectionBuilder;

public class SAXTariffCollectionBuilder extends TariffCollectionBuilder {
	
	private static final Logger LOG = Logger.getLogger(SAXTariffCollectionBuilder.class);
	private SAXTariffHandler handler;
	private XMLReader reader;
	
	public XMLReader getReader() {
		return reader;
	}

	public SAXTariffCollectionBuilder() throws TariffException {
		handler = new SAXTariffHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();										// Создаём объект-парсер вида SAX
			reader.setContentHandler(handler);													// Определяем для парсера наш хендлер в качестве обработчика содержиого
			reader.setErrorHandler(handler);													// Определяем для парсера наш хендлер в качестве обработчика ошибок
		} catch (SAXException ex) {
			throw new TariffException("Error with getting reader-object for SAX parser", ex);
		}
	}
	
	@Override
	public void buildTariffSet(String fileName) {
		try {
			InputStream input = new FileInputStream(fileName);
			InputSource source = new InputSource(input);
			reader.parse(source);														// Запускаем парсинг нужного хмл-файла
		} catch (IOException ex) {
			LOG.error("Some I/O exception has occured while parsing with SAX-parser" + "\n" + ex);
		} catch (SAXException ex) {
			LOG.error("Some SAX exception has occured while parsing with SAX-parser" + "\n" + ex);
		}
		tariffSet = handler.getTariffSet();
		deniedTariffs = handler.getDeniedTariffs();
	}

}
