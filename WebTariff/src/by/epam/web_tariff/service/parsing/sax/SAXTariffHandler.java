package by.epam.web_tariff.service.parsing.sax;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.web_tariff.entity.CallParameterName;
import by.epam.web_tariff.entity.InternetParameterName;
import by.epam.web_tariff.entity.Tariff;
import by.epam.web_tariff.entity.TariffName;
import by.epam.web_tariff.service.parsing.TariffsTagName;
import by.epam.web_tariff.service.AddService;
import by.epam.web_tariff.service.ValidationService;

@SuppressWarnings("incomplete-switch")
public class SAXTariffHandler extends DefaultHandler {
		
	private static final Logger LOG = Logger.getLogger(SAXTariffHandler.class);
	
	private Set<Tariff> tariffSet;
	private Map<Tariff, String> deniedTariffs;
	
	// поля для внутренних промежуточных вычислений
	private StringBuilder text;
	private boolean nativeFlag;
	private boolean stationaryFlag;
	private boolean othernetFlag;
	private String wrongTariffName;
	
	// список полей из классов, которые будут принимать в себя значения в процессе парсинга 
	private String tariffId;											// Tariff
	private String operatorName;
	private TariffName tariffName;
	private int connectionFee;
	private int initialPayment;
	private int internetSpeedDefault;
	private int smsPrice;
	private int mmsPrice;
	private Map<CallParameterName, Object> nativePriceTable;
	private Map<CallParameterName, Object> stationaryPriceTable;
	private Map<CallParameterName, Object> otherNetworkPriceTable;
	private int everyMegabytePrice;										// CallTariff
	private Map<InternetParameterName, Object> internetPriceTable; 		// InternetTariff
	private int internetSpeedMax;
		
	public SAXTariffHandler() {
		tariffSet = new HashSet<Tariff>();
		deniedTariffs = new HashMap<Tariff, String>();
		nativePriceTable = new HashMap<CallParameterName, Object>();
		stationaryPriceTable = new HashMap<CallParameterName, Object>();
		otherNetworkPriceTable = new HashMap<CallParameterName, Object>();
		internetPriceTable = new HashMap<InternetParameterName, Object>();
	}
	
	public Set<Tariff> getTariffSet() {
		return tariffSet;
	}
	
	public Map<Tariff, String> getDeniedTariffs() {
		return deniedTariffs;
	}
	
	@Override
	public void startDocument() {
		LOG.info("SAX Parsing process has started");
	}
	
	public void endDocument() {
		LOG.info("SAX Parsing process has come to an end");
	}
	
	public void startElement(String uri, String localName, String qName, 
			Attributes attributes) {
		
		LOG.debug("start element -> " + "uri: " + uri + ", localNmae: " + localName
				+ ", qName: " + qName);
		text = new StringBuilder();
		
		switch (localName) {
		case "call-tariff":
		case "internet-tariff":
			this.tariffId = attributes.getValue("id");
			break;
		case "native-network":
			this.nativeFlag = true;
			break;		
		case "stationary-network":
			this.stationaryFlag = true;
			break;
		case "other-network":
			this.othernetFlag = true;
			break;
		}
	}
	
	public void characters(char[] buffer, int start, int length) {
		text.append(buffer, start, length);
	}
	
	public void endElement(String uri, String localName, String qName) {
		String negativeValidationReason = null;
		String reason = null;
		boolean insertResult = false;
		TariffsTagName tagName = TariffsTagName.valueOf(localName.toUpperCase().replace("-", "_"));

		switch(tagName) {
		case OPERATOR_NAME:
			operatorName = text.toString();
			break;
		case TARIFF_NAME:
			try {
				tariffName = TariffName.valueOf(text.toString().toUpperCase().replace(' ', '_'));
			} catch (IllegalArgumentException ex) {
				tariffName = TariffName.UNKNOWN;
				wrongTariffName = text.toString();
			}
			break;
		case CONNECTION_FEE:
			connectionFee = Integer.parseInt(text.toString());
			break;
		case INITIAL_PAYMENT:
			initialPayment = Integer.parseInt(text.toString());
			break;
		case INTERNET_SPEED_DAFAULT:
			internetSpeedDefault = Integer.parseInt(text.toString());
			break;
		case INTERNET_SPEED_MAX:
			internetSpeedMax = Integer.parseInt(text.toString());
			break;
		case EVERY_MEGABYTE_PRICE:
			everyMegabytePrice = Integer.parseInt(text.toString());
			break;
		case SMS:
			smsPrice = Integer.parseInt(text.toString());
			break;
		case MMS:
			mmsPrice = Integer.parseInt(text.toString());
			break;		
		case MINUTES_WITHOUT_DISCOUNT:
			if (nativeFlag) {
				nativePriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				stationaryPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				otherNetworkPriceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(text.toString()));
			}
			break;
		case MINUTES_WITH_DISCOUNT:
			if (nativeFlag) {
				nativePriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				stationaryPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				otherNetworkPriceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			}
			break;
		case DISCOUNT_MINUTE_PRICE:
			if (nativeFlag) {
				nativePriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				stationaryPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				otherNetworkPriceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(text.toString()));
			}
			break;
		case COMMON_MINUTE_PRICE:
			if (nativeFlag) {
				nativePriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (stationaryFlag) {
				stationaryPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text.toString()));
			} else if (othernetFlag) {
				otherNetworkPriceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(text.toString()));
			}
			break;
		case MINUTE_TYPE:
			if (nativeFlag) {
				nativePriceTable.put(CallParameterName.MINUTE_TYPE, text.toString());
			} else if (stationaryFlag) {
				stationaryPriceTable.put(CallParameterName.MINUTE_TYPE, text.toString());
			} else if (othernetFlag) {
				otherNetworkPriceTable.put(CallParameterName.MINUTE_TYPE, text.toString());
			}
			break;
		case MEGABYTES_WITH_DISCOUNT:
			internetPriceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.parseInt(text.toString()));
			break;
		case DISCOUNT_MEGABYTE_PRICE:
			internetPriceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.parseInt(text.toString()));
			break;
		case COMMON_MEGABYTE_PRICE:
			internetPriceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.parseInt(text.toString()));
			break;
		case AMOUNT_OF_TRAFFIC:
			internetPriceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.parseInt(text.toString()));
			break;
		case AMOUNT_OF_FAST_TRAFFIC:
			internetPriceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.parseInt(text.toString()));
			break;
		case MEGABYTE_TYPE:
			internetPriceTable.put(InternetParameterName.MEGABYTE_TYPE, text.toString());
			break;
		case NATIVE_NETWORK:
			nativeFlag = false;
			break;
		case STATIONARY_NETWORK:
			stationaryFlag = false;
			break;
		case OTHER_NETWORK:
			othernetFlag = false;
			break;
		case CALL_TARIFF:
			if (tariffName == TariffName.UNKNOWN) {
				LOG.info("Tariff " + wrongTariffName + " denied!");
				reason = "wrong tariff name";
				AddService.addDeniedTariff(deniedTariffs, reason, tariffId, operatorName, tariffName, connectionFee, initialPayment,		// если тариф с неизвестным именем, добавляем его в набор отклонённых тарифов 
									 internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
									 nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
				break;
			}
			negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment,		// отправляем данные на валидацию 
													 			  internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
													 			  nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			if (negativeValidationReason == null) {																							// если валидация прошла успешно, отправляем данные сервису вставки тарифа в набор
				insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 			// валидация считается успешной, если причина отказа осталась равной null
													internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
													nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);		
				LOG.debug("Validation result is true!");
				if (insertResult) {
					LOG.debug("Insertion result is true!");
					LOG.info("Tariff " + tariffName + " accepted!");
				} else {
					LOG.debug("Insertion result is false!");
					LOG.info("Tariff " + tariffName + " denied!");
				}
				
			} else {
				LOG.debug("Validation result is false!");
				LOG.info("Tariff " + tariffName + " denied!");
				AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 		// добавляем неправильный тариф в набор отклонённых тарифов 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
						 				   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			}
			clearVariables(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, smsPrice, mmsPrice,		// обнуляем значения всех временных полей 
						   everyMegabytePrice, nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			break;
		case INTERNET_TARIFF:
			if (tariffName == TariffName.UNKNOWN) {
				LOG.info("Tariff " + wrongTariffName + " denied!");
				AddService.addDeniedTariff(deniedTariffs, reason, tariffId, operatorName, tariffName, connectionFee, initialPayment,		// если тариф с неизвестным именем, добавляем его в набор отклонённых тарифов 
						 				   internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
						 				   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
				break;
			}
			negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment,		// отправляем данные на валидацию 
													internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax,  
													nativePriceTable, stationaryPriceTable, otherNetworkPriceTable,
													internetPriceTable);
			if (negativeValidationReason == null) {																							// если валидация прошла успешно, отправляем данные сервису вставки тарифа в набор
				insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 			// валидация считается успешной, если причина отказа осталась равной null
													internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax,  
													nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, 
													internetPriceTable);
				LOG.debug("Validation result is true!");
				if (insertResult) {
					LOG.debug("Insertion result is true!");
					LOG.info("Tariff " + tariffName + " accepted!");
				} else {
					LOG.debug("Insertion result is false!\n");
					LOG.info("Tariff " + tariffName + " denied!");
				}
			} else {
				LOG.debug("Validation result is false!");
				LOG.info("Tariff " + tariffName + " denied!");
				AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee,		// добавляем неправильный тариф в набор отклонённых тарифов 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			}
			clearVariables(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, smsPrice, mmsPrice,		// обнуляем значения всех временных полей 
						   internetSpeedMax, nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
			break;
		}		
	}
	
	// метод по обнулению всех полей, которые используются при парсинге call-tariff-ов
	private void clearVariables(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
								   int internetSpeedDefault, int smsPrice, int mmsPrice, int everyMegabytePrice,  
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable) {
		tariffId = null; 
		operatorName = null; 
		tariffName = null; 
		connectionFee = 0; initialPayment = 0; internetSpeedDefault = 0; smsPrice = 0; mmsPrice = 0; everyMegabytePrice = 0;
		nativePriceTable.clear(); stationaryPriceTable.clear(); otherNetworkPriceTable.clear();
	}
	
	// метод по обнулению всех полей, которые используются при парсинге internet-tariff-ов
	private void clearVariables(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, 
								   int internetSpeedDefault, int internetSpeedMax, int smsPrice, int mmsPrice,
								   Map<CallParameterName, Object> nativePriceTable, 
								   Map<CallParameterName, Object> stationaryPriceTable, 
								   Map<CallParameterName, Object> otherNetworkPriceTable, 
								   Map<InternetParameterName, Object> internetPriceTable) {
		tariffId = null; 
		operatorName = null; 
		tariffName = null; 
		connectionFee = 0; initialPayment = 0; internetSpeedDefault = 0; smsPrice = 0; mmsPrice = 0; internetSpeedMax = 0;
		nativePriceTable.clear(); stationaryPriceTable.clear(); otherNetworkPriceTable.clear(); internetPriceTable.clear();
	}
	
	public void warning(SAXParseException exception) {
		LOG.warn("WARNING: line " + exception.getLineNumber() + ": "
				+ exception.getMessage() + "\n");
	}
	
	public void error(SAXParseException exception) {
		LOG.error("ERROR: line " + exception.getLineNumber() + ": "
				+ exception.getMessage());
	}
	
	public void fatalError(SAXParseException exception) throws SAXParseException {
		LOG.fatal("FATAL: line " + exception.getLineNumber() + ": "
				+ exception.getMessage());
		throw(exception);
	}

}
