package by.epam.web_tariff.service.parsing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import by.epam.web_tariff.entity.Tariff;

public abstract class TariffCollectionBuilder {
	
	protected Set<Tariff> tariffSet;
	protected Map<Tariff, String> deniedTariffs;
	
	public TariffCollectionBuilder() {
		tariffSet = new HashSet<Tariff>();
		deniedTariffs = new HashMap<Tariff, String>();
	}
	
	public abstract void buildTariffSet(String fileName);
	
	public Set<Tariff> getTariffSet() {
		return tariffSet;
	}
	
	public Map<Tariff, String> getDeniedTariffs() {
		return deniedTariffs;
	}

}
