package by.epam.web_tariff.service.parsing.dom;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import by.epam.web_tariff.entity.CallParameterName;
import by.epam.web_tariff.entity.InternetParameterName;
import by.epam.web_tariff.entity.TariffName;
import by.epam.web_tariff.exception.TariffException;
import by.epam.web_tariff.service.parsing.TariffCollectionBuilder;
import by.epam.web_tariff.service.AddService;
import by.epam.web_tariff.service.ValidationService;

public class DOMTariffCollectionBuilder extends TariffCollectionBuilder {
	
	private static final Logger LOG = Logger.getLogger(DOMTariffCollectionBuilder.class);
	private DOMParser parser;
	
	public DOMTariffCollectionBuilder() {
		parser = new DOMParser();															// создаём DOM-парсер (Xerces)
	}
	
	// метод, запускающий парсинг документа и формирование коллекции тарифов
	@Override
	public void buildTariffSet(String fileName) {
		LOG.info("DOM parsing process has started");
		try {
			InputStream input = new FileInputStream(fileName);
			InputSource source = new InputSource(input);
			parser.parse(source);															// парсим наш хмл-файл
		} catch (IOException ex) {
			LOG.error("Some I/O exception has occured while parsing with DOM-parser");
		} catch (SAXException ex) {
			LOG.error("Some SAX exception has occured while parsing with DOM-parser");
		}
		
		Document document = parser.getDocument();											// получаем из парсера наш документ в виде DOM-дерева
		LOG.debug("DOM parsing process has come to an end");
		
		Element root = document.getDocumentElement();										// получаем первый (корневой) элемент из документа
		NodeList tariffNodes = root.getChildNodes();										// создаём список узлов дерева

		for(int i = 1; i < tariffNodes.getLength(); i+=2) {									// забираем каждый 2-ой узел списка и присваиваем его объекту типа Element
			Element tariffElement = (Element) tariffNodes.item(i);							// каждый второй, потому что среди childов помимо самих элементов есть ещё и пробелы (отступы) между элементами
			try {																			// они тоже идут как элементы (text), но нам они не нужны для создания объектов. Поэтому их обходим!
				buildTariff(tariffElement);													// отправляем элемент методу для добавления в коллецию тарифов
			} catch (TariffException ex) {
				LOG.error(ex.getMessage());
			}
		}
		LOG.info("DOM parsing process has come to an end");
	}
	
	// метод, создающий тариф и обеспечивающий его попадание в коллекцию
	private void buildTariff(Element tariffElement) throws TariffException {
		boolean insertResult = false;
		String value = null;
		String wrongTariffName = null;
		String negativeValidationReason = null;
		String tagName = tariffElement.getTagName();
				
		// список полей, которые принимают в себя значения элементов дерева
		String tariffId = null;																					// Tariff
		String operatorName = null;
		TariffName tariffName = null;
		int connectionFee = 0;
		int initialPayment = 0;
		int internetSpeedDefault = 0;
		Map<CallParameterName, Object> nativePriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> stationaryPriceTable = new HashMap<CallParameterName, Object>();
		Map<CallParameterName, Object> otherNetworkPriceTable = new HashMap<CallParameterName, Object>();
		int smsPrice = 0;
		int mmsPrice = 0;
		int everyMegabytePrice = 0;																				// CallTariff
		Map<InternetParameterName, Object> internetPriceTable = new HashMap<InternetParameterName, Object>(); 	// InternetTariff
		int internetSpeedMax = 0;	
		
		LOG.debug("tariff at present moment: " + tariffElement.getTagName() + " " + tariffElement.getAttribute("id"));
		tariffId = tariffElement.getAttribute("id");															// получаем значения для общих полей тарифа
		operatorName = getTextContentOfChildElement(tariffElement, "operator-name");
		try {
			value = getTextContentOfChildElement(tariffElement, "tariff-name");
			tariffName = TariffName.valueOf(value.toUpperCase().replace(" ", "_"));
		} catch (IllegalArgumentException ex) {
			wrongTariffName = value;
			tariffName = null;
		}
		connectionFee = Integer.parseInt(getTextContentOfChildElement(tariffElement, "connection-fee"));
		initialPayment = Integer.parseInt(getTextContentOfChildElement(tariffElement, "initial-payment"));
		internetSpeedDefault = Integer.parseInt(getTextContentOfChildElement(tariffElement, "internet-speed-dafault"));
		nativePriceTable = getPriceTable(tariffElement, "native-network");
		stationaryPriceTable = getPriceTable(tariffElement, "stationary-network");
		otherNetworkPriceTable = getPriceTable(tariffElement, "other-network");
		smsPrice = Integer.parseInt(getTextContentOfChildElement(tariffElement, "sms"));
		mmsPrice = Integer.parseInt(getTextContentOfChildElement(tariffElement, "mms"));
		
		switch (tagName) {
		case "call-tariff":
			everyMegabytePrice = Integer.parseInt(getTextContentOfChildElement(tariffElement, "every-megabyte-price"));					// получаем значения для полей конкретного вида тарифа
			
			negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
														  		  internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
														  		  nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			if (negativeValidationReason == null) {
				insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
													internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
													nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
				LOG.debug("Validation result is true!");
				if (insertResult) {
					LOG.debug("Insertion result is true!");
					LOG.info("Tariff " + tariffName + " accepted!");
				} else {
					LOG.debug("Insertion result is false!");
					LOG.info("Tariff " + tariffName + " denied!");
				}
				
			} else {
				LOG.debug("Validation result is false!");
				if (wrongTariffName != null) {
					LOG.info("Tariff " + wrongTariffName + " denied!");
					tariffName = TariffName.UNKNOWN;
				} else {
					LOG.info("Tariff " + tariffName + " denied!");
				}
				AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee, 	// добавляем неправильный тариф в набор отклонённых тарифов 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			}
			break;
		case "internet-tariff":
			internetPriceTable = getPriceTable(tariffElement);																			// получаем значения для полей конкретного вида тарифа
			internetSpeedMax = Integer.parseInt(getTextContentOfChildElement(tariffElement, "internet-speed-max"));
			
			negativeValidationReason = ValidationService.validate(tariffId, operatorName, tariffName, connectionFee, initialPayment, 
														  internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, nativePriceTable, 
														  stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
			if (negativeValidationReason == null) {
				insertResult = AddService.addTariff(tariffSet, tariffId, operatorName, tariffName, connectionFee, initialPayment, 
													internetSpeedDefault, smsPrice, mmsPrice, internetSpeedMax, nativePriceTable, 
													stationaryPriceTable, otherNetworkPriceTable, internetPriceTable);
				LOG.debug("Validation result is true!");
				if (insertResult) {
					LOG.debug("Insertion result is true!");
					LOG.info("Tariff " + tariffName + " accepted!");
				} else {
					LOG.debug("Insertion result is false!");
					LOG.info("Tariff " + tariffName + " denied!");
				}

			} else {
				LOG.debug("Validation result is false!\n");
				if (wrongTariffName != null) {
					LOG.info("Tariff " + wrongTariffName + " denied!");
					tariffName = TariffName.UNKNOWN;
				} else {
					LOG.info("Tariff " + tariffName + " denied!");
				}
				AddService.addDeniedTariff(deniedTariffs, negativeValidationReason, tariffId, operatorName, tariffName, connectionFee,	// добавляем неправильный тариф в набор отклонённых тарифов 
										   initialPayment, internetSpeedDefault, smsPrice, mmsPrice, everyMegabytePrice, 
										   nativePriceTable, stationaryPriceTable, otherNetworkPriceTable);
			}
			break;
		default:
			throw new TariffException("Unknown xml-element was found in the element \"tariffs\"");
		}
	}
	
	// метод для получения содержимого дочернего элемента
	private static String getTextContentOfChildElement(Element element, String childName) {
		String textContent = null;
		NodeList nodeList = element.getElementsByTagName(childName);
		textContent = nodeList.item(0).getTextContent().trim();;
		return textContent;
	}
	
	// метод для получения дочернего элемента
	private static Element getChildElement(Element element, String childName) {
		NodeList nodeList = element.getElementsByTagName(childName);
		Element childElement = (Element) nodeList.item(0);
		return childElement;
	}
	
	// метод для считывания цен на звонки в определённой сети
	private Map<CallParameterName, Object> getPriceTable(Element tariffElement, String networkType) throws TariffException {
		Element element = null;
		NodeList nodeList = null;
		String textContent = null;
		String nodeName = null;
		Map<CallParameterName, Object> priceTable = new HashMap<CallParameterName, Object>();
		
		element = getChildElement(getChildElement(tariffElement, "call-prices"), networkType);		// получаем либо элемент native-network, либо stationary-network, либо other-network
		nodeList = element.getChildNodes();															// получаем все имеющиеся в нём дочерние элементы
		
		for (int i = 1; i < nodeList.getLength(); i+=2) {											// для каждого дочернего элемента получаем его содержимое
			nodeName = nodeList.item(i).getNodeName();
			textContent = getTextContentOfChildElement(element, nodeName);
			switch (nodeName) {																		// в зависимости от того, каким оказался дочерний элемент, преобразуем полученное содержимое к нужному формату
			case "minutes-without-discount":
				priceTable.put(CallParameterName.MINUTES_WITHOUT_DISCOUNT, Integer.parseInt(textContent));
				break;
			case "minutes-with-discount":
				priceTable.put(CallParameterName.MINUTES_WITH_DISCOUNT, Integer.parseInt(textContent));
				break;
			case "discount-minute-price":
				priceTable.put(CallParameterName.DISCOUNT_MINUTE_PRICE, Integer.parseInt(textContent));
				break;
			case "common-minute-price":
				priceTable.put(CallParameterName.COMMON_MINUTE_PRICE, Integer.parseInt(textContent));
				break;
			case "minute-type":
				priceTable.put(CallParameterName.MINUTE_TYPE, textContent);
				break;
			default:
				throw new TariffException("Unknown xml-element was found in the element \"call-prices\"");
			}
		}				
		return priceTable;
	}
	
	// перегруженная версия метода getPriceTable() для считывания цен на интернет
	private Map<InternetParameterName, Object> getPriceTable(Element tariffElement) throws TariffException {
		Element element = null;
		NodeList nodeList = null;
		String textContent = null;
		String nodeName = null;
		Map<InternetParameterName, Object> priceTable = new HashMap<InternetParameterName, Object>();
		
		element = getChildElement(tariffElement, "mobile-internet");								// получаем элемент mobile-internet
		nodeList = element.getChildNodes();															// получаем все имеющиеся в нём дочерние элементы
		
		for (int i = 1; i < nodeList.getLength(); i+=2) {											// для каждого дочернего элемента получаем его содержимое
			nodeName = nodeList.item(i).getNodeName();
			textContent = getTextContentOfChildElement(element, nodeName);
			switch (nodeName) {																		// в зависимости от того, каким оказался дочерний элемент, преобразуем полученное содержимое к нужному формату
			case "megabytes-with-discount":
				priceTable.put(InternetParameterName.MEGABYTES_WITH_DISCOUNT, Integer.parseInt(textContent));
				break;
			case "discount-megabyte-price":
				priceTable.put(InternetParameterName.DISCOUNT_MEGABYTE_PRICE, Integer.parseInt(textContent));
				break;
			case "common-megabyte-price":
				priceTable.put(InternetParameterName.COMMON_MEGABYTE_PRICE, Integer.parseInt(textContent));
				break;
			case "amount-of-traffic":
				priceTable.put(InternetParameterName.AMOUNT_OF_TRAFFIC, Integer.parseInt(textContent));
				break;
			case "amount-of-fast-traffic":
				priceTable.put(InternetParameterName.AMOUNT_OF_FAST_TRAFFIC, Integer.parseInt(textContent));
				break;
			case "megabyte-type":
				priceTable.put(InternetParameterName.MEGABYTE_TYPE, textContent);
				break;
			default:
				throw new TariffException("Unknown xml-element was found in the element \"mobile-internet\"");
			}
		}
		return priceTable;
	}
}
