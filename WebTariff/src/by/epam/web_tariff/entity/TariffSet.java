package by.epam.web_tariff.entity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class TariffSet {
	
	private Set<Tariff> tariffSet;
	
	public TariffSet() {
		tariffSet = new HashSet<Tariff>();
	}
	
	public boolean addToTariffSet(Tariff tariff) {
		return tariffSet.add(tariff);
	}

	// метод, возвращающий клон инкапсулированного поля
	public Set<Tariff> getTariffSetClone() {
		Set<Tariff> clone = new HashSet<Tariff>();
		for (Tariff tariff : this.tariffSet) {
			clone.add(tariff);
		}
		return clone;
	}
	
	public Set<Tariff> getTariffSet() {
		return Collections.unmodifiableSet(tariffSet);
	}
}
