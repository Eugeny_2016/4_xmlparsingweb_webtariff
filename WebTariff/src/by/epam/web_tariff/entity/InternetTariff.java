package by.epam.web_tariff.entity;

import java.util.Collections;
import java.util.Map;

public class InternetTariff extends Tariff {
	
	private Map<InternetParameterName, Object> internetPriceTable;
	private int internetSpeedMax;
	
	public InternetTariff() {
		super();
	}
	
	public InternetTariff(String tariffId, String operatorName, TariffName tariffName, int connectionFee,
			int initialPayment, int internetSpeedDefault, int smsPrice, int mmsPrice, int internetSpeedMax, 
			Map<CallParameterName, Object> nativePriceTable,
			Map<CallParameterName, Object> stationaryPriceTable,
			Map<CallParameterName, Object> otherNetworkPriceTable,
			Map<InternetParameterName, Object> internetPriceTable) {
		
		super(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault,
				nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, smsPrice, mmsPrice);
		
		this.internetPriceTable = internetPriceTable;
		this.internetSpeedMax = internetSpeedMax;
		
	}
	
	public Map<InternetParameterName, Object> getInternetPriceTable() {
		return Collections.unmodifiableMap(internetPriceTable);
	}

	public int getInternetSpeedMax() {
		return internetSpeedMax;
	}

	public void setInternetPriceTable(Map<InternetParameterName, Object> internetPriceTable) {
		this.internetPriceTable = internetPriceTable;
	}
	
	public void setInternetSpeedMax(int internetSpeedMax) {
		this.internetSpeedMax = internetSpeedMax;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append(super.toString());
		str.append(" Internet prices: ").append(internetPriceTable);
		str.append(" Internet maximum speed: ").append(internetSpeedMax);
		
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return (int) (super.hashCode() + internetPriceTable.hashCode()*7 + internetSpeedMax*10);
	}

	@Override
	public boolean equals(Object obj) {
		InternetTariff internetTariff = (InternetTariff) obj;
		
		if (!super.equals(obj)) {
			return false;
		}
		
		if (!this.internetPriceTable.equals(internetTariff.internetPriceTable)) {
			return false;
		}
		
		if (internetSpeedMax != internetTariff.internetSpeedMax) {
			return false;
		}
		
		return true;
	}

}
