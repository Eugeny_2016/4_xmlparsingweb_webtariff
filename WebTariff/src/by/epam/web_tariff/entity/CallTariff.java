package by.epam.web_tariff.entity;

import java.util.Map;

public class CallTariff extends Tariff {
		
	private int everyMegabytePrice;
	
	public CallTariff() {
		super();
	}
	
	public CallTariff(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment,
					  int internetSpeedDefault,int smsPrice, int mmsPrice, int everyMegabytePrice, 
					  Map<CallParameterName, Object> nativePriceTable, 
					  Map<CallParameterName, Object> stationaryPriceTable, 
					  Map<CallParameterName, Object> otherNetworkPriceTable) {
		
		super(tariffId, operatorName, tariffName, connectionFee, initialPayment, internetSpeedDefault, 
				nativePriceTable, stationaryPriceTable, otherNetworkPriceTable, smsPrice, mmsPrice);
		
		this.everyMegabytePrice = everyMegabytePrice;
	}
		
	public int getEveryMegabytePrice() {
		return everyMegabytePrice;
	}

	public void setEveryMegabytePrice(int everyMegabytePrice) {
		this.everyMegabytePrice = everyMegabytePrice;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append(super.toString());
		str.append(" Every megabyte price: ").append(everyMegabytePrice);
		
		return str.toString();
	}
	
	@Override
	public int hashCode() {
		return (int) (super.hashCode() + everyMegabytePrice*31);
	}

	@Override
	public boolean equals(Object obj) {
		CallTariff callTariff = (CallTariff) obj;
		
		if (!super.equals(obj)) {
			return false;
		}
					
		if (this.everyMegabytePrice != callTariff.everyMegabytePrice) {
			return false;
		}
		
		return true;
	}
}
