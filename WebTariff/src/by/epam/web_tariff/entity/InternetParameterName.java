package by.epam.web_tariff.entity;

public enum InternetParameterName {
	MEGABYTES_WITH_DISCOUNT,
	DISCOUNT_MEGABYTE_PRICE,
	COMMON_MEGABYTE_PRICE,
	AMOUNT_OF_TRAFFIC,
	AMOUNT_OF_FAST_TRAFFIC,
	MEGABYTE_TYPE;
}
