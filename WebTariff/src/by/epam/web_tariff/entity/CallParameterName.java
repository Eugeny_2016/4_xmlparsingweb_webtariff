package by.epam.web_tariff.entity;

public enum CallParameterName {
	MINUTES_WITHOUT_DISCOUNT,
	MINUTES_WITH_DISCOUNT,
	DISCOUNT_MINUTE_PRICE,
	COMMON_MINUTE_PRICE,
	MINUTE_TYPE;
}
