package by.epam.web_tariff.entity;

import java.util.Collections;
import java.util.Map;

public abstract class Tariff {
	
	private String tariffId;
	private String operatorName;
	private TariffName tariffName;
	private int connectionFee;
	private int initialPayment;
	private int internetSpeedDefault;
	private Map<CallParameterName, Object> nativePriceTable;
	private Map<CallParameterName, Object> stationaryPriceTable;
	private Map<CallParameterName, Object> otherNetworkPriceTable;
	private int smsPrice;
	private int mmsPrice;
	
	public Tariff() {}
	
	public Tariff(String tariffId, String operatorName, TariffName tariffName, int connectionFee, int initialPayment, int internetSpeedDefault, 
			Map<CallParameterName, Object> nativePriceTable, Map<CallParameterName, Object> stationaryPriceTable, 
			Map<CallParameterName, Object> otherNetworkPriceTable, int smsPrice, int mmsPrice) {
		
		this.tariffId = tariffId;
		this.operatorName = operatorName;
		this.tariffName = tariffName;
		this.connectionFee = connectionFee;
		this.initialPayment = initialPayment;
		this.internetSpeedDefault = internetSpeedDefault;
		this.nativePriceTable = nativePriceTable;
		this.stationaryPriceTable = stationaryPriceTable;
		this.otherNetworkPriceTable = otherNetworkPriceTable;
		this.smsPrice = smsPrice;
		this.mmsPrice = mmsPrice;
	}
		
	public String getTariffId() {
		return tariffId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public TariffName getTariffName() {
		return tariffName;
	}

	public int getConnectionFee() {
		return connectionFee;
	}
	
	public int getInitialPayment() {
		return initialPayment;
	}
	
	public int getInternetSpeedDefault() {
		return internetSpeedDefault;
	}
	
	public Map<CallParameterName, Object> getNativePriceTable() {
		return Collections.unmodifiableMap(nativePriceTable);
	}

	public Map<CallParameterName, Object> getStationaryPriceTable() {
		return Collections.unmodifiableMap(stationaryPriceTable);
	}

	public Map<CallParameterName, Object> getOtherNetworkPriceTable() {
		return Collections.unmodifiableMap(otherNetworkPriceTable);
	}
	
	public int getSmsPrice() {
		return smsPrice;
	}

	public int getMmsPrice() {
		return mmsPrice;
	}

	public void setTariffId(String id) {
		this.tariffId = id;
	}
	
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public void setTariffName(TariffName tariffName) {
		this.tariffName = tariffName;
	}

	public void setConnectionFee(int connectionFee) {
		this.connectionFee = connectionFee;
	}
	
	public void setInitialPayment(int initialPayment) {
		this.initialPayment = initialPayment;
	}
	
	public void setInternetDefaultSpeed(int internetSpeedDefault) {
		this.internetSpeedDefault = internetSpeedDefault;
	}

	public void setNativePriceTable(Map<CallParameterName, Object> nativePriceTable) {
		this.nativePriceTable = nativePriceTable;
	}

	public void setStationaryPriceTable(Map<CallParameterName, Object> stationaryPriceTable) {
		this.stationaryPriceTable = stationaryPriceTable;
	}

	public void setOtherNetworkPriceTable(Map<CallParameterName, Object> otherNetworkPriceTable) {
		this.otherNetworkPriceTable = otherNetworkPriceTable;
	}
	
	public void setSmsPrice(int smsPrice) {
		this.smsPrice = smsPrice;
	}

	public void setMmsPrice(int mmsPrice) {
		this.mmsPrice = mmsPrice;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Tariff's id: ").append(tariffId);
		str.append(" Operator name: ").append(operatorName);
		str.append(" Tariff name: ").append(tariffName);
		str.append(" Connection fee: ").append(connectionFee);
		str.append(" Initial payment: ").append(initialPayment);
		str.append(" Internet default speed: ").append(internetSpeedDefault);
		str.append(" Native network call prices: ").append(nativePriceTable);
		str.append(" Stationary network call prices: ").append(stationaryPriceTable);
		str.append(" Other networks call prices: ").append(otherNetworkPriceTable);
		str.append(" SMS price: ").append(smsPrice);
		str.append(" MMS price: ").append(mmsPrice);
		return str.toString();
	}

	@Override
	public int hashCode() {
		return (int) (tariffId.hashCode()*31 + operatorName.hashCode()*31 + tariffName.hashCode()*25 + connectionFee*9
				+ initialPayment*18 + internetSpeedDefault*11 + nativePriceTable.hashCode()*10 + stationaryPriceTable.hashCode()*24
				+ otherNetworkPriceTable.hashCode()*7 + smsPrice*5 + mmsPrice*31);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Tariff tariff = (Tariff) obj;
		
		if (!tariffId.equals(tariff.tariffId)) {
			return false;
		}
		
		if (!operatorName.equals(tariff.operatorName)) {
			return false;
		}
		
		if (!tariffName.equals(tariff.tariffName)) {
			return false;
		}
		
		if (connectionFee != tariff.connectionFee) {
			return false;
		}
		
		if (initialPayment != tariff.initialPayment) {
			return false;
		}
		
		if (internetSpeedDefault != tariff.internetSpeedDefault) {
			return false;
		}
		
		if (!this.nativePriceTable.equals(tariff.nativePriceTable)) {
			return false;
		}
		
		if (!this.stationaryPriceTable.equals(tariff.stationaryPriceTable)) {
			return false;
		}
		
		if (!this.otherNetworkPriceTable.equals(tariff.otherNetworkPriceTable)) {
			return false;
		}
		
		if (smsPrice != tariff.smsPrice) {
			return false;
		}
		
		if (mmsPrice != tariff.mmsPrice) {
			return false;
		}
		
		return true;
	}
	
}
