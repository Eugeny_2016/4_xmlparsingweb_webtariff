package by.epam.web_tariff.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.web_tariff.command.Command;
import by.epam.web_tariff.command.CommandMatcher;

public class FrontController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String COMMAND_PARAMETER = "command";
		
	public FrontController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CommandMatcher commandMatcher = CommandMatcher.getInstance();
		String commandValue = request.getParameter(COMMAND_PARAMETER);
		Command command = commandMatcher.getCommand(commandValue);
		
		String page = (command != null) ? command.execute(request) : JspPageName.ERROR_PAGE;
		
		request.getRequestDispatcher(page).forward(request, response);			
	}	
}
