package by.epam.web_tariff.controller;

public class JspPageName {
	private JspPageName() {}
	
	public static final String INDEX_PAGE = "index.jsp";
	public static final String ERROR_PAGE = "/jsp/error/error.jsp";
	public static final String AFTER_PARSING_PAGE = "/jsp/afterParsingPage.jsp";

}
