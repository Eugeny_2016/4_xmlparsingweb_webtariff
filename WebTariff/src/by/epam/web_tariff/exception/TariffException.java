package by.epam.web_tariff.exception;

public class TariffException extends Exception {

	private static final long serialVersionUID = 1L;

	public TariffException() {
		super();
	}

	public TariffException(String message, Throwable ex) {
		super(message, ex);
	}

	public TariffException(String message) {
		super(message);
	}

	public TariffException(Throwable ex) {
		super(ex);
	}	
}
