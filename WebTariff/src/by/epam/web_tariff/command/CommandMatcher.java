package by.epam.web_tariff.command;

import java.util.HashMap;
import java.util.Map;

import by.epam.web_tariff.command.impl.ParseCommand;

public final class CommandMatcher {
	
	private static final CommandMatcher instance = new CommandMatcher();
	
	private Map<CommandName, Command> table = new HashMap<>();
	
	private CommandMatcher() {
		table.put(CommandName.PARSE, new ParseCommand());
	}
	
	public static CommandMatcher getInstance() {
		return instance;
	}
	
	public Command getCommand(String commandName) {
		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		Command command = null;
		if( name != null) {
			command = table.get(name);
		}
		return command;
	}	
}
