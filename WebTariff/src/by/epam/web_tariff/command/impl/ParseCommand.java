package by.epam.web_tariff.command.impl;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.web_tariff.service.XSDValidationService;
import by.epam.web_tariff.command.Command;
import by.epam.web_tariff.controller.JspPageName;
import by.epam.web_tariff.entity.CallTariff;
import by.epam.web_tariff.entity.InternetTariff;
import by.epam.web_tariff.entity.Tariff;
import by.epam.web_tariff.exception.TariffException;
import by.epam.web_tariff.service.SelectingService;
import by.epam.web_tariff.service.parsing.TariffCollectionBuilder;
import by.epam.web_tariff.service.parsing.factory.TariffCollectionBuilderFactory;

public class ParseCommand implements Command {
	
	private static final Logger LOG = Logger.getLogger(ParseCommand.class);
	private static final String PARSER_CHOICE_REQUEST_PARAMETER = "parser_choice";
	private static final String TARIFFS_XML_PATH_CONTEXT_PARAMETER = "tariffsXMLPath";
	private static final String TARIFFS_XSD_PATH_CONTEXT_PARAMETER = "tariffsXSDPath";
		
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String fileName = null;
		String schemaName = null;
		String parserChoice = null;
		TariffCollectionBuilder builder = null;
		TariffCollectionBuilderFactory factory = null;
		Set<Tariff> callTariffSet = null;
		Set<Tariff> internetTariffSet = null;
		Map<Tariff, String> deniedTariffs = null;
		
		fileName = request.getServletContext().getInitParameter(TARIFFS_XML_PATH_CONTEXT_PARAMETER);
		schemaName = request.getServletContext().getInitParameter(TARIFFS_XSD_PATH_CONTEXT_PARAMETER);
		parserChoice = request.getParameter(PARSER_CHOICE_REQUEST_PARAMETER);										// выбор для radio-element может быть только один, поэтому получаем именно одно значение, а не массив
		LOG.debug("Parser choice: " + parserChoice + " fileName: " + fileName + " schemaName" + schemaName);
		
		// Если документ валидный, то начинаем его парсить с целью создания коллекции тарифов
		if (XSDValidationService.validateByXSD(request.getServletContext().getRealPath(fileName), request.getServletContext().getRealPath(schemaName))) {
			factory = new TariffCollectionBuilderFactory();
			try {
				builder = factory.getTariffBuilder(parserChoice);
			} catch (TariffException ex) {
				throw new RuntimeException(ex);
			}
			
			builder.buildTariffSet(request.getServletContext().getRealPath(fileName));
			
			callTariffSet = SelectingService.selectTariffsByClass(builder.getTariffSet(), CallTariff.class);
			internetTariffSet = SelectingService.selectTariffsByClass(builder.getTariffSet(), InternetTariff.class);
			deniedTariffs = builder.getDeniedTariffs();
					
			request.setAttribute("callTariffSet", callTariffSet);
			request.setAttribute("internetTariffSet", internetTariffSet);
			request.setAttribute("deniedTariffs", deniedTariffs);
			request.setAttribute("parserChoice", parserChoice);
			page = JspPageName.AFTER_PARSING_PAGE;
			
		} else {
			page = JspPageName.ERROR_PAGE;
		}		
		return page;		
	}
}
